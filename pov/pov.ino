long period = 400;

/*******Led Configuration******
*/
//int ledPins[6][3] = {{4, 3, 2}, {7, 6, 5}, {10, 9, 8}, {13, 12, 11}, {14,15,16}, {19, 18, 17}};
/***Going from top LED to bottom LED****/
int ledPins[6][3] = {  {2,3,4}, {5,6,7},{8,9,10}, {11,12,13}, {14,15,16}, {17,18,19} };
//int ledPins[1][3] = {17,18,19};

int n = 42;

char *message[6] = {
"22222222222222333333333333333222222222222222",
"22222222222233311111111111133322222222222222",
"22222222222331177777117777733332222222222222",
"22222222222233111111111113333332222222222222",
"22222222222223337777777773333222222222222222",
"22222222222223333333333333333222222222222222"
};


//Hello Message
//char *message[6] = {
//"440004400111111100220000055000000666660000",
//"440004400110000000220000055000006600066000",
//"444444400111110000220000055000006600066000",
//"440004400110000000220000055000006600066000",
//"440004400111111100222220055555000666660000",
//"000000000000000000000000000000000000000000"
//};

//char *mario[6] = {
//"77777777777777777777777777777777777777777777777777777777770000000000000111777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777",
//"77777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777771111001011777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777",
//"77777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777",
//"77777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777106666666660177777777777777777777777777777777777777777777777777777777777777777777777777777777777777777",
//"77777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777",
//"33333333333333331111111177777773000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
//};

//Sets all pins to output
void setup()
{
  Serial.begin(9600);
 for (int led = 0; led < 6; led ++)
 {
   for (int color = 0; color < 3; color++)
   {
     pinMode(ledPins[led][color], OUTPUT);
   }
 }
}

void loop()
{
  for (int col = 0; col < n; col++)
  {
    for (int row = 0; row < 6; row++)
    {
      int color = (int)(message [row][col] - '0');
      Serial.println(color);
      setLed(row, color); 
    }

   // delay(50);
  }
  delayMicroseconds(period);
   allOff();
  delayMicroseconds(period / 16);
  
}

//Eight cases:  All on, All off(white), red, green, blue, magenta, yellow, cyan 
void setLed(int led, int color)
{
  int temp = color;
  switch(temp){
//TODO : Switch case 1 and 0 to proper order
    //White
    case 0:
      //Serial.println("here1");
      digitalWrite(ledPins[led][0], 0);
      digitalWrite(ledPins[led][1], 0);
      digitalWrite(ledPins[led][2], 0);
      break;
    //LED Off
    case 1:
      // Serial.println("here1");
      digitalWrite(ledPins[led][0], 1);
      digitalWrite(ledPins[led][1], 1);
      digitalWrite(ledPins[led][2], 1);
      break;
    //Red
    case 2:
      digitalWrite(ledPins[led][0], 0);
      digitalWrite(ledPins[led][1], 1);
      digitalWrite(ledPins[led][2], 1);
      break;
    //Green
    case 3:
      digitalWrite(ledPins[led][0],  1);
      digitalWrite(ledPins[led][1],  0);
      digitalWrite(ledPins[led][2],  1);
      break;
    //Blue
    case 4:
      digitalWrite(ledPins[led][0], 1);
      digitalWrite(ledPins[led][1], 1);
      digitalWrite(ledPins[led][2], 0);
      break;
    //Magenta
    case 5:
      digitalWrite(ledPins[led][0], 0);
      digitalWrite(ledPins[led][1], 1);
      digitalWrite(ledPins[led][2], 0);
      break;
    //Yellow
    case 6:
      digitalWrite(ledPins[led][0], 0);
      digitalWrite(ledPins[led][1], 0);
      digitalWrite(ledPins[led][2], 1);
      break;
    //Cyan
    case 7:
      digitalWrite(ledPins[led][0], 1);
      digitalWrite(ledPins[led][1], 0);
      digitalWrite(ledPins[led][2], 0);
      break;
  }
  return;
}

void allOff()
{
   for (int led = 0; led < 6; led ++)
 {
   for (int color = 0; color < 3; color++)
   {
     digitalWrite(ledPins[led][color], 1);
   }
 }
}
