#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>

using namespace cv;
using namespace std;

struct color{
  int b;
  int g;
  int r;
};

color white, black, red, green, blue, magenta, yellow, cyan;

void setup(){
  white = {255, 255, 255};
  black = {0, 0 ,0};
  red = {0, 0, 255};
  green = {0, 255, 0};
  blue = {255, 0, 0};
  magenta = {255, 0 , 255};
  yellow ={0, 255, 255};
  cyan = {255, 255, 0};
}


int main( int argc, char** argv )
{

  setup();
  //Reads the input image and puts into src
  Mat src;
  src = imread(argv[1], 1);


  Size newSize(200, 6);
  //Size newSize(500, 500);
  Mat dst;//= Mat::zeros( src.size(), CV_8UC3 );

  //Resizes dst image to new size
  resize(src,dst,newSize);

  std::ofstream out( "output.txt" );

  //Changing the images color to only be the 8 colors available
  for(int row=0; row < dst.rows; ++row){
     out << "\"";
    for(int col=0; col < dst.cols; ++col){
      //We are going to try to get the smallest difference
      int difference = 1000;
      int temp;
      char colorValue;
      //Format B G R
      Vec3b currentColor = dst.at<Vec3b>(Point(col,row));
      int b = currentColor.val[0];
      int g = currentColor.val[1];
      int r = currentColor.val[2];

      Vec3b newColor;

      temp = abs(abs(white.b - b) + abs(white.g - g) + abs(white.r - r));
      if(temp < difference)
        { difference = temp; newColor = {255, 255, 255}; colorValue = '0';}

      temp = (abs(black.b - b) + abs(black.g - g) + abs(black.r - r));
      if(temp < difference)
        {difference = temp; newColor = {0, 0 ,0}; colorValue = '1';}

      temp = (abs(red.b - b) + abs(red.g - g) + abs(red.r - r));
      if(temp < difference)
        {difference = temp; newColor = {0, 0, 255}; colorValue = '2';}

      temp = (abs(green.b - b) + abs(green.g - g) + abs(green.r - r));
      if(temp < difference)
        {difference = temp; newColor = {0, 255, 0}; colorValue = '3';}

      temp = (abs(blue.b - b) + abs(blue.g - g) + abs(blue.r - r));
      if(temp < difference)
        {difference = temp; newColor = {255, 0, 0}; colorValue = '4';}

      temp = (abs(magenta.b - b) + abs(magenta.g - g) + abs(magenta.r - r));
      if(temp < difference)
        {difference = temp; newColor = {255, 0 , 255}; colorValue = '5';}

      temp = (abs(yellow.b - b) + abs(yellow.g - g) + abs(yellow.r - r));
      if(temp < difference)
        {difference = temp; newColor = {0, 255, 255}; colorValue = '6';}

      temp = (abs(cyan.b - b) + abs(cyan.g - g) + abs(cyan.r - r));
      if(temp < difference)
        {difference = temp; newColor = {255, 255, 0}; colorValue = '7';}

      //Must check eight different colors
      out << colorValue;
      dst.at<Vec3b>(Point(col,row)) = newColor;
    }

    //out << "\n";
    //If not in the last row
    if( row != (dst.rows - 1) ){
      out << "\"";
      out << ",";
      out << "\n";
    }
    else
      out << "\"";
  }

  // namedWindow( "Original Image", CV_WINDOW_AUTOSIZE );
  // imshow( "Original Image", src );
  //
  // namedWindow( "New Colors", CV_WINDOW_AUTOSIZE );
  // imshow( "New Colors", dst );

  waitKey(0);
  return(0);
  //return 0;
}
